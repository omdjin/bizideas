const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');

// Load Models
require('./models/Idea');
require('./models/User');

// Passport config
require('./config/passport')(passport);

// Load routes
const auth = require('./routes/auth');
const ideas = require('./routes/ideas');
const index = require('./routes/index');

// Load keys
const keys = require('./config/keys');

// Handlebars Helpers
const {
  truncate,
  stripTags,
  formatDate,
  select,
  editIcon
} = require('./helpers/hbs');

// Set Global Promise
mongoose.Promise = global.Promise;

// Connect mongoose
mongoose.connect(keys.mongoURI).then(() => {
  console.log('MongoDB Connected!');
}).catch(err =>console.log(err));

const app = express();

// Body parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Method Override Middleware
app.use(methodOverride('_method'));

// Handlebars middleware
app.engine('handlebars', exphbs({
  helpers: {
    truncate: truncate,
    stripTags: stripTags,
    formatDate: formatDate,
    select: select,
    editIcon: editIcon
  },
  defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

// User cookie parser and express session
app.use(cookieParser());
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false
}));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

// Set global vars
app.use((req, res, next) => {
  res.locals.user = req.user || null;
  next();
})

// Set static folders
app.use(express.static(path.join(__dirname, 'public')));

// Use routes
app.use('/auth', auth);
app.use('/ideas', ideas);
app.use('/', index);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server start on port: ${port}`);
});
