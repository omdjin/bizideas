## About

This is a Node/Express app to share public or write private business ideas.

Version: 1.0.0

## Features

- Login with google
- Create Public, Private, and Unpublished ideas
- Post comments
- User access controll

## Get Started

1. **Clone this project**. `git clone https://gitlab.com/omdjin/vidjot.git`

2. **Install dependencies**. `npm install`

3. **Start the project** `npm start`

### Live Demo

[Click Here!](https://stark-cliffs-73316.herokuapp.com/)
